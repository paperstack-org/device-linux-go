GITLAB_URL=gitlab.com
GROUP_NAME=sdu-labcloud
MODULE_NAME=node-linux-go
REGISTRY=registry.gitlab.com
ARCH?=arm
CI_COMMIT_SHA?=dev
BRANCH=$(shell git rev-parse --abbrev-ref HEAD)

.PHONY: run
run:
	go run -ldflags "-X $(GITLAB_URL)/$(GROUP_NAME)/$(MODULE_NAME)/internal/system.CommitSHA=$(CI_COMMIT_SHA)" main.go

.PHONY: build
build:
	go build -ldflags "-s -w -X $(GITLAB_URL)/$(GROUP_NAME)/$(MODULE_NAME)/internal/system.CommitSHA=$(CI_COMMIT_SHA)" -v -o $(MODULE_NAME)-$(ARCH) main.go

.PHONY: arm
arm:
	docker run --rm -ti -v "$(shell pwd)":/app $(REGISTRY)/$(GROUP_NAME)/$(MODULE_NAME)/toolchain:arm /usr/bin/qemu-arm-static /bin/sh -c "cd /app && go build -o $(MODULE_NAME)-arm main.go"

.PHONY: download
download:
	wget -q --show-progress https://gitlab.com/$(GROUP_NAME)/$(MODULE_NAME)/-/jobs/artifacts/$(BRANCH)/raw/$(MODULE_NAME)-$(ARCH)?job=compile-$(ARCH) -O $(MODULE_NAME)-$(ARCH)
	chmod +x $(MODULE_NAME)-$(ARCH)

.PHONY: toolchain
toolchain:
	docker login $(REGISTRY)
	docker build -t $(REGISTRY)/$(GROUP_NAME)/$(MODULE_NAME)/toolchain:arm -f arm.Dockerfile .
	docker push $(REGISTRY)/$(GROUP_NAME)/$(MODULE_NAME)/toolchain:arm
	docker logout $(REGISTRY)

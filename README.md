# Node Linux (Go)

[![Go Report](https://goreportcard.com/badge/gitlab.com/sdu-labcloud/node-linux-go)](https://goreportcard.com/report/gitlab.com/sdu-labcloud/node-linux-go)

A reference implementation, written in [Go][golang], for an IoT node using the platform.

## Installation

The application comes with an installation script located at `https://gitlab.com/sdu-labcloud/node-linux-go/-/raw/master/scripts/install.sh`. If you like living on the edge, you can install the application as shown below:

```shell
$ wget -q https://gitlab.com/sdu-labcloud/node-linux-go/-/raw/master/scripts/install.sh -O - | sudo bash -
```

## Compilation

The project uses a `Makefile` and Docker for cross-compilation. Make sure that both are installed and available in your `$PATH`. To build the binary for the Raspberry Pi 3, run `make arm`.

You can also locally build the application on a machine, that does not have any of the required hardware. For this, run `make local`. To locally build and run the application, run `make`.

## Usage

Make sure to have a `.env` file in the same folder as the binary when you execute the application:

```ini
# The API URL of the LabCloud REST API server.
LABCLOUD_API_URL=https://api.thecore.sdu.dk
# Enable logging of the LEDs color value for the mocked implementation.
MOCK_LED_ENABLE=true
# Mock serial number.
MOCK_SERIAL=123456
```

## Simulation

On platforms other than `arm` that do not support a card reader, the card reader can be simulated. To do this, make sure that you have a file named `.env` in the same directory as your binary. It should have the following content:

```ini
MOCK_RFID_UID=INSERT_DESIRED_RFID_UID
```

While the variable has a value, the value will be used as the RFID UID. It can be updated during runtime.

## Toolchain

Currently, the image for the cross-compilation has to be manually built and uploaded to the registry of the project in GitLab. You can do this, by running: `make toolchain`

## License

This project is licensed under the terms of the [MIT License](./LICENSE.md).

[golang]: https://golang.org/

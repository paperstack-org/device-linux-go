# Build C library bindings for LED driver.
FROM balenalib/armv7hf-golang:latest-build AS build
RUN [ "cross-build-start" ]
WORKDIR /tmp
RUN apt-get update -y && apt-get install -y scons
RUN git clone https://github.com/jgarff/rpi_ws281x.git && cd rpi_ws281x && scons
RUN [ "cross-build-end" ]

# Assemble toolchain Docker image with required static libraries and header files.
FROM balenalib/armv7hf-golang:latest
RUN [ "cross-build-start" ]
COPY --from=build /tmp/rpi_ws281x/*.a /usr/local/lib/
COPY --from=build /tmp/rpi_ws281x/*.h /usr/local/include/
RUN [ "cross-build-end" ]

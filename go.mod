module gitlab.com/sdu-labcloud/node-linux-go

go 1.13

require (
	github.com/inconshreveable/go-update v0.0.0-20160112193335-8152e7eb6ccf
	github.com/joho/godotenv v1.3.0
	github.com/rpi-ws281x/rpi-ws281x-go v1.0.5
	github.com/stianeikeland/go-rpio/v4 v4.4.0
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20200908134130-d2e65c121b96 // indirect
	gopkg.in/yaml.v2 v2.3.0 // indirect
)

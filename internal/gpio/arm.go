// +build arm

package gpio

import (
	"log"
	"time"

	"github.com/stianeikeland/go-rpio/v4"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/tmpstore"
)

var accessControlPin rpio.Pin
var lockStatePin rpio.Pin
var lightStripPin1 rpio.Pin
var lightStripPin2 rpio.Pin
var lockLogicType rpio.State

// Define the constants used to specify the lock type
const (
	// LogicTypeSolenoid is locked on logical high.
	LogicTypeSolenoid = "solenoid"
	// LogicTypeMotor is locked on logical low
	LogicTypeMotor = "motor"
  )

// init will initialize the memory for the GPIO access.
func init() {
	// Map memory for GPIO access.
	err := rpio.Open()
	if err != nil {
		log.Fatalf("error: initializing GPIOs failed: %s", err)
	}

	// Configure the GPIO for the lock or the relay.
	accessControlPin = rpio.Pin(23)
	accessControlPin.Output()
	accessControlPin.Low()

	// Configure the GPIO for the lock state monitor.
	lockStatePin = rpio.Pin(24)
	lockStatePin.Input()
	lockStatePin.Pull(rpio.PullUp)

	// Configure the GPIO for the LED strip.
	// Use two pins for both MOSFETs on the breakout PCB.
	lightStripPin1 = rpio.Pin(25)
	lightStripPin1.Output()
	lightStripPin1.Low()

	lightStripPin2 = rpio.Pin(8)
	lightStripPin2.Output()
	lightStripPin2.Low()

	cache := tmpstore.Get()

	// Check if self test was made.
	if cache.Peripherals.LockType == "" {
		log.Println("GPIO: previous self test not found ...")

		// Determine lock type
		lockLogicType = LockLogicSelfTest()

		// convert gpio state to appropriate lock type
		switch lockLogicType {
		case rpio.High:
			cache.Peripherals.LockType = LogicTypeSolenoid
		case rpio.Low:
			cache.Peripherals.LockType = LogicTypeMotor
		}
		// save lock type
		tmpstore.Set(cache)
	} else {
		log.Println("GPIO: previous self test detected ...")

		//convert lock type to appropriate gpio state
		switch cache.Peripherals.LockType {
		case LogicTypeSolenoid:
			lockLogicType = rpio.High
		case LogicTypeMotor:
			lockLogicType = rpio.Low
		}
	}

	log.Printf("GPIO: using %s lock type", cache.Peripherals.LockType)
}

// LockLogicSelfTest actuates the lock and watches the state of the lock pin to determine the lock sense logic.
// Returns the pin state when locked.
func LockLogicSelfTest() rpio.State {
	// Read initial pin state
	initialState := lockStatePin.Read()

	// Actuate the lock
	log.Printf("LockLogicSelfTest: Actuating lock")
	LockOpen()

	if lockStatePin.Read() == initialState {
		// Lock state has not changed, so the initial state was open.
		// We XOR the state with 1, returning the opposite value
		return initialState ^ 1
	} else {
		// Lock state has changed, so the initial state was locked.
		return initialState
	}
}

// LockOpen opens the lock by giving a small pulse.
func LockOpen() {
	accessControlPin.High()
	time.Sleep(1 * time.Second)
	accessControlPin.Low()
}

// RelayOn closes the relay allowing current to flow.
func RelayOn() {
	accessControlPin.High()
}

// RelayOff opens the relay preventing current from flowing.
func RelayOff() {
	accessControlPin.Low()
}

// LightStripOn turns on the LED strip mosfets.
func LightStripOn() {
	lightStripPin1.High()
	lightStripPin2.High()
}

// LightStripOff turns off the LED strip mosfets.
func LightStripOff() {
	lightStripPin1.Low()
	lightStripPin2.Low()
}

// LockState returns the state of the lock. True if locked, false if open.
func LockState() bool {
	return lockStatePin.Read() == lockLogicType
}

// Reset will reset the GPIO driver and free the mapped memory.
func Reset() {
	accessControlPin.Low()
	err := rpio.Close()
	if err != nil {
		log.Fatalf("error: resetting GPIOs failed: %s", err)
	}
}

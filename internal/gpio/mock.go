// +build !arm

package gpio

import (
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/sdu-labcloud/node-linux-go/internal/tmpstore"
)

var lockLogicType string

// init is a mock initialize for retreving settings from .env file
func init() {
	log.Println("GPIO: using mock driver")
	cache := tmpstore.Get()

	// Check if self test was made.
	if cache.Peripherals.LockType == "" {
		log.Println("GPIO: previous self test not found ...")

		// read mock lock type in .env file
		lockType := strings.TrimSpace(os.Getenv("MOCK_LOCK_TYPE"))

		// check if lockType contains any data
		if lockType == "" {
			log.Println("GPIO: set MOCK_LOCKTYPE to 'solenoid' or 'motor' to test tmpstore")
			// set known value if lock type not found
			cache.Peripherals.LockType = "solenoid"
		} else {
			cache.Peripherals.LockType = lockType
		}

		// save new lock type
		tmpstore.Set(cache)

	} else {
		log.Println("GPIO: previous self test detected (reset tmp to set new type) ...")
	}

	lockLogicType = cache.Peripherals.LockType
	log.Printf("GPIO: using %s lock type", lockLogicType)
}

// LockOpen is a mock displaying the actions on the lock.
func LockOpen() {
	log.Printf("Lock: Open")
	time.Sleep(1 * time.Second)
	log.Printf("Lock: Closed")
}

// RelayOn is a mock to indicate the state of the relay.
func RelayOn() {
	log.Printf("Relay: On")
}

// RelayOff is a mock to indicate the state of the relay.
func RelayOff() {
	log.Printf("Relay: Off")
}

// LightStripOn is a mock to indicate the state of the LED strip
func LightStripOn() {
	log.Printf("LEDs: On")
}

// LightStripOff is a mock to indicate the state of the LED strip
func LightStripOff() {
	log.Printf("LEDs: Off")
}

// LockState returns the state of the lock. True if locked, false if open.
func LockState() bool {
	return true
}

// Reset is a mock to provide the same interface across platforms.
func Reset() {
	// Do nothing.
}

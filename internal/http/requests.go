package http

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

// Store pointer to shared HTTP client.
var httpClient *http.Client

// Token is used when making HTTP requests that require authentication
var Token string

// init will create a shared HTTP client.
func init() {
	// Store HTTP client for use in multiple requests.
	httpClient = &http.Client{}
}

const (
	// Get is a constant for the GET HTTP verb.
	Get = "GET"
	// Post is a constant for the POST HTTP verb.
	Post = "POST"
	// Patch is a constant for the PATCH HTTP verb.
	Patch = "PATCH"
)

// Request makes an HTTP request.
func Request(method string, url string, payload io.Reader) []byte {
	// Configure HTTP request with headers.
	req, err := http.NewRequest(method, url, payload)
	if err != nil {
		log.Fatalf("error: creating HTTP request failed: %s", err)
	}
	req.Header.Set("User-Agent", "device-linux-go/0.1.0")
	req.Header.Set("Content-Type", "application/json")

	if Token != "" {
		req.Header.Set("Authorization", "JWT "+Token)
	}

	// Send HTTP request and receive response.
	res, err := httpClient.Do(req)
	if err != nil {
		log.Fatalf("error: sending HTTP request failed: %s", err)
	}

	// Read response body.
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("error: reading HTTP response body failed: %s", err)
	}

	// Close response body.
	err = res.Body.Close()
	if err != nil {
		log.Fatalf("error: closing HTTP response body failed: %s", err)
	}

	return body
}

// JSONRequest makes an HTTP request and attempts to parse the response as JSON.
func JSONRequest(method string, url string, payload interface{}) interface{} {
	// Make HTTP request.
	var res []byte
	if payload != nil {
		// Convert payload to JSON if not empty.
		payloadBytes, err := json.Marshal(payload)
		if err != nil {
			log.Fatalf("error: encoding HTTP request as JSON failed: %s", err)
		}
		res = Request(method, url, bytes.NewBuffer(payloadBytes))
	} else {
		res = Request(method, url, nil)
	}

	// Parse response body.
	var jsonRes interface{}
	err := json.Unmarshal(res, &jsonRes)
	if err != nil {
		log.Fatalf("error: decoding HTTP response as JSON failed: %s", err)
	}

	// Return parsed JSON.
	return jsonRes
}

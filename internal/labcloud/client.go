package labcloud

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/http"
)

// Store API URL.
var apiURL string

// init will ensure that the required configuration is loaded.
func init() {
	// Load environment variables from ".env" file.
	if err := godotenv.Load(); err != nil {
		log.Fatalf("error: loading .env file failed: %s", err)
	}

	// Cache API URL.
	apiURL = strings.TrimSpace(os.Getenv("LABCLOUD_API_URL"))
	if apiURL == "" {
		log.Fatal("error: environment variable missing: LABCLOUD_API_URL")
	}
}

// Health returns the health of the API server.
func Health() (res HealthResponse) {
	// Get information from health endpoint.
	json.Unmarshal(http.Request(http.Get, apiURL+"/v1/health", nil), &res)
	return
}

// CreateOrReadNode creates a new node, which has to be
// unique by its combination of MAC and serial number.
// If the node already exists, it will read the node from the API.
func CreateOrReadNode(nodeInfo NodeInfo) Node {
	// Create a node.
	nodeJSON, err := json.Marshal(nodeInfo)
	if err != nil {
		log.Fatalf("error: encoding HTTP request as JSON failed: %s", err)
	}

	// Make HTTP call.
	res := http.Request(http.Post, apiURL+"/v1/nodes", bytes.NewBuffer(nodeJSON))

	// Attempt to unmarshal to node.
	var nodeRes NodeResponse
	var node Node
	err = json.Unmarshal(res, &nodeRes)

	// An error occurred.
	if nodeRes.Data.ID == "" {
		// Check if a conflict occurred.
		var conflictRes ConflictResponse
		err = json.Unmarshal(res, &conflictRes)
		if err != nil {
			log.Fatalf("error: decoding HTTP response as JSON failed: %s", err)
		}

		if conflictRes.ID == "" {
			log.Fatalf("error: " + conflictRes.Error)
		}

		// Read existing node.
		node = ReadNode(conflictRes.ID)
	} else {
		log.Println("Created node: " + nodeRes.Data.ID)
		node = nodeRes.Data
	}

	return node
}

// ReadNode reads the node identified by the ID.
func ReadNode(ID string) Node {
	// Read a node.
	var temp NodeResponse
	json.Unmarshal(http.Request(http.Get, apiURL+"/v1/nodes/"+ID, nil), &temp)

	return temp.Data
}

// UpdateNode updates the node identified by the ID.
func UpdateNode(ID string, node Node) Node {
	// Update a node.

	update := NodeApplicationUpdate{
		ApplicationVersion: node.ApplicationVersion,
		ApplicationName:    node.ApplicationName,
	}

	updateJSON, err := json.Marshal(update)
	if err != nil {
		log.Fatalf("error: encoding HTTP request as JSON failed: %s", err)
	}

	var temp NodeResponse
	json.Unmarshal(http.Request(http.Patch, apiURL+"/v1/nodes/"+ID, bytes.NewBuffer(updateJSON)), &temp)

	return temp.Data
}

// CreateSession creates a new session for the node specified by the ID
func CreateSession(ID string) Session {
	// Create new session for a node.
	var temp SessionResponse
	json.Unmarshal(http.Request(http.Post, apiURL+"/v1/nodes/"+ID+"/sessions", nil), &temp)

	// Save token in a global variable
	http.Token = temp.Data.Token

	return temp.Data
}

// ReadSession reads the properties of an existing session
func ReadSession(ID string, sessionID string) Session {
	// Read existing session.
	var temp SessionResponse
	json.Unmarshal(http.Request(http.Get, apiURL+"/v1/nodes/"+ID+"/sessions/"+sessionID, nil), &temp)

	return temp.Data
}

// ReadPermission reads the properties of a permission
func ReadPermission(ID string) Permission {
	// Read permission.
	var temp PermissionResponse
	json.Unmarshal(http.Request(http.Get, apiURL+"/v1/permissions/"+ID, nil), &temp)

	return temp.Data
}

// ReadMachine reads the properties of a machine
func ReadMachine(ID string) Machine {
	// Read machine.
	var temp MachineResponse
	json.Unmarshal(http.Request(http.Get, apiURL+"/v1/machines/"+ID, nil), &temp)

	return temp.Data
}

// UpdateMachineStatus updates the status of a machine
func UpdateMachineStatus(ID string, status string) Machine {
	// Update machine
	machineStatus := MachineStatus{
		Status: status,
	}
	statusJSON, err := json.Marshal(machineStatus)
	if err != nil {
		log.Fatalf("error: encoding HTTP request as JSON failed: %s", err)
	}

	var temp MachineResponse
	json.Unmarshal(http.Request(http.Patch, apiURL+"/v1/machines/"+ID, bytes.NewBuffer(statusJSON)), &temp)

	// TODO: Investigate, why here the response does not contain a Machine.

	return temp.Data
}

// UpdateMachineUser updates the user of the machine and sets the status to busy.
func UpdateMachineUser(ID string, userID string) Machine {
	// Update user of machine.
	machineUser := MachineUser{
		User:   userID,
		Status: "busy",
	}
	userJSON, err := json.Marshal(machineUser)
	if err != nil {
		log.Fatalf("error: encoding HTTP request as JSON failed: %s", err)
	}

	var temp MachineResponse
	json.Unmarshal(http.Request(http.Patch, apiURL+"/v1/machines/"+ID, bytes.NewBuffer(userJSON)), &temp)

	return temp.Data
}

// ReadUser reads the properties of a user
func ReadUser(ID string) User {
	// Read user
	var temp UserResponse
	json.Unmarshal(http.Request(http.Get, apiURL+"/v1/users/"+ID, nil), &temp)

	return temp.Data
}

// ReadLatestApplication will return the properties of an application
func ReadLatestApplication(applicationName string, releaseChannel string) Application {
	// Read latest application
	var temp ApplicationResponse
	var applicationInfo string
	applicationInfo = fmt.Sprintf("/v1/applications/%s?cpu_architecture=arm&release_channel=%s", applicationName, releaseChannel)
	json.Unmarshal(http.Request(http.Get, apiURL+applicationInfo, nil), &temp)

	return temp.Data
}

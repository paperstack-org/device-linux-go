package labcloud

// NodeInfo describes a device that is managed by the LabCloud platform.
type NodeInfo struct {
	ApplicationVersion string `json:"application_version"`
	ApplicationName    string `json:"application_name"`
	MACAddress         string `json:"mac_address"`
	SerialNumber       string `json:"serial_number"`
	OperatingSystem    string `json:"operating_system"`
}

// NodeApplicationUpdate describes the application version of the node.
type NodeApplicationUpdate struct {
	ApplicationVersion string `json:"application_version"`
	ApplicationName    string `json:"application_name"`
}

// HealthResponse describes the state of the server
type HealthResponse struct {
	Started string `json:"started"`
	Tag     string `json:"tag"`
}

// Node describes the properties of a node
type Node struct {
	ApplicationName    string   `json:"application_name"`
	ApplicationVersion string   `json:"application_version"`
	Capabilities       []string `json:"capabilities"`
	Created            string   `json:"created"`
	ID                 string   `json:"id"`
	MACAddress         string   `json:"mac_address"`
	Machine            string   `json:"machine"`
	OperatingSystem    string   `json:"operating_system"`
	ReleaseChannel     string   `json:"release_channel"`
	Roles              []string `json:"roles"`
	SerialNumber       string   `json:"serial_number"`
	Sessions           []string `json:"sessions"`
	Updated            string   `json:"updated"`
}

// NodeResponse describes the properties of an existing node
type NodeResponse struct {
	Data Node `json:"data"`
}

// Session describes the properties of a session
type Session struct {
	Authorized bool   `json:"authorized"`
	Created    string `json:"created"`
	ID         string `json:"id"`
	Identify   bool   `json:"identify"`
	Node       string `json:"node"`
	Seen       string `json:"seen"`
	Token      string `json:"token"`
	Updated    string `json:"updated"`
}

// SessionResponse is an intermediate struct for the properties of a session
type SessionResponse struct {
	Data Session `json:"data"`
}

// Permission describes the properties of a permission
type Permission struct {
	Description  string `json:"description"`
	Scheduling   string `json:"scheduling"`
	Workflow     string `json:"workflow"`
	Default      bool   `json:"open"`
	Abbreviation string `json:"abbreviation"`
	Name         string `json:"name"`
	Image        string `json:"image"`
	Created      string `json:"created"`
	Updated      string `json:"updated"`
	ID           string `json:"id"`
}

// PermissionResponse is an intermediate struct for the properties of a permission
type PermissionResponse struct {
	Data Permission `json:"data"`
}

// Machine describes the properties of a machine
type Machine struct {
	Permission string   `json:"permission"`
	Status     string   `json:"status"`
	Node       string   `json:"node"`
	User       string   `json:"user"`
	Job        string   `json:"job"`
	Number     int      `json:"number"`
	Attributes []string `json:"attributes"`
	Created    string   `json:"created"`
	Updated    string   `json:"updated"`
	ID         string   `json:"id"`
}

// MachineResponse is an intermediate struct for the properties of a machine
type MachineResponse struct {
	Data Machine `json:"data"`
}

// MachineStatus describes the status of a machine
type MachineStatus struct {
	Status string `json:"status"`
}

// MachineUser describes the user of a machine
type MachineUser struct {
	User   string `json:"user"`
	Status string `json:"status"`
}

// User describes the properties of a user
type User struct {
	ID          string   `json:"id"`
	AzureID     string   `json:"azure_id"`
	CardID      string   `json:"card_id"`
	CardNumber  string   `json:"card_number"`
	Created     string   `json:"created"`
	DisplayName string   `json:"display_name"`
	Email       string   `json:"email"`
	FirstName   string   `json:"first_name"`
	LastName    string   `json:"last_name"`
	Permissions []string `json:"permissions"`
	Role        string   `json:"role"`
	Updated     string   `json:"updated"`
}

// UserResponse is an intermediate struct for the properties of a user
type UserResponse struct {
	Data User `json:"data"`
}

// ConflictResponse occurs if an entity already exists.
type ConflictResponse struct {
	Error string `json:"error"`
	ID    string `json:"id"`
}

// Application describes the properties of an application
type Application struct {
	Application  string `json:"application"`
	CommitSHA    string `json:"commit_sha"`
	DownloadHref string `json:"download_href"`
}

// ApplicationResponse is an intermediate struct for the properties of a user
type ApplicationResponse struct {
	Data Application `json:"data"`
}

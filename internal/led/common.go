package led

import (
	"fmt"
	"log"
	"time"
)

// currentAnimation indicates which animation is currently being
// displayed. It is used to prevent accidental interruptions of
// animations.
var currentAnimation string

// identifyState indicates if the node is currently in "identify" mode.
// If it is, no other animations will be played
var identifyState bool

// animationChange is a channel that controls the goroutine for the led controller.
var animationChange chan bool

// init will configure the logger to use the UTC time format in logs.
// This is required as this will execute before the main function.
// Without this the first log entry would be relative to the local
// timezone.
func init() {
	log.SetFlags(log.LstdFlags | log.LUTC)
	animationChange = make(chan bool)

	// Set a static color upon startup
	go ledRoutineStatic(ColorPurple)
}

// resetAnimation checks if the next requested animation is identical.
// Returns true if the animation is different and was overwritten and
// false if the animations are identical.
func resetAnimation(nextAnimation string) bool {
	if currentAnimation != nextAnimation {
		currentAnimation = nextAnimation
		return true
	}
	return false
}

func ledRoutineBlink(color uint32) {
	var count int
	state := false
	for {
		select {
		case <-animationChange:
			return
		default:
			if count%10 == 0 {
				state = !state
			}
			if state {
				render(color)
			} else {
				render(ColorBlack)
			}
		}
		time.Sleep(time.Millisecond * 50)
		count++
	}
}

func ledRoutineStatic(color uint32) {
	for {
		select {
		case <-animationChange:
			return
		default:
			render(color)
		}
	}
}

// Static will set the LED to a constant color.
func Static(color uint32) {
	if identifyState {
		return
	}
	nextAnimation := fmt.Sprintf("static.0x%08X", color)
	if resetAnimation(nextAnimation) {
		animationChange <- true
		go ledRoutineStatic(color)
	}
}

// Blink will set the LED to a certain color, then blink it.
func Blink(color uint32) {
	if identifyState {
		return
	}
	nextAnimation := fmt.Sprintf("blink.0x%08X", color)
	if resetAnimation(nextAnimation) {
		animationChange <- true
		go ledRoutineBlink(color)
	}
}

// Identify will blink the LED a known color and override any other LED calls.
func Identify(identify bool) {
	if identify {
		nextAnimation := "Identify"
		identifyState = true
		if resetAnimation(nextAnimation) {
			animationChange <- true
			go ledRoutineBlink(ColorPurple)
		}
	} else {
		identifyState = false
	}
}

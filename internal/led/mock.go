// +build !arm

package led

import (
	"log"
	"os"
	"strings"
)

var enableLED bool

// init will indicate which driver is being used.
func init() {
	log.Println("LED: using mock driver")

	// Check if mocked LED output should be suppressed.
	enableLED = strings.TrimSpace(os.Getenv("MOCK_LED_ENABLE")) == "true"
	if !enableLED {
		log.Println("LED: set MOCK_LED_ENABLE=true to log LED color values")
	}
}

// render will flush the color to the LED.
func render(color uint32) {
	// Display which color is being rendered.
	if enableLED {
		log.Printf("LED: 0x%08X\n", color)
	}
}

// Reset is needed to provide the same interface as
// on an ARM architecture.
func Reset() {
	// Do nothing.
}

package logic

import (
	"log"
	"os"
	"time"

	"gitlab.com/sdu-labcloud/node-linux-go/internal/labcloud"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/system"
)

var machineID string
var initialized bool

// RegisterNode will register the node against the API.
// During this process the node will ensure that
// it exists in the API.
func RegisterNode() labcloud.Node {

	// Gather system information.
	serialNumber := system.Serial()
	macAddress := system.NormalizeMAC(&system.ActiveNetworkInterface().HardwareAddr)

	// Combine information about device.
	nodeInfo := labcloud.NodeInfo{
		ApplicationVersion: system.CommitSHA,
		ApplicationName:    "node-linux-go",
		MACAddress:         macAddress,
		SerialNumber:       serialNumber,
		OperatingSystem:    "linux",
	}

	return labcloud.CreateOrReadNode(nodeInfo)
}

// CheckNodeMachine continuously monitors the node
// to verify that it has a machine assigned to it.
// It will end the application if the node does
// not have a machine assigned or if it changes.
func CheckNodeMachine(mutexes *system.Mutexes, node *labcloud.Node) {
	// Check if the machine has changed.
	for {
		mutexes.Node.Lock()

		// Fetch node from API.
		updatedNode := labcloud.ReadNode(node.ID)
		if updatedNode.ID != "" {
			*node = updatedNode
		}

		// Check if machine IDs match.
		if machineID != node.Machine {
			mutexes.Update.Lock()
			log.Println("Machine changed!")
			os.Exit(0)
		}
		mutexes.Node.Unlock()

		time.Sleep(5 * time.Minute)
	}
}

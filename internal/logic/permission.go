package logic

import (
	"time"

	"gitlab.com/sdu-labcloud/node-linux-go/internal/labcloud"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/system"
)

// SyncPermission ensures that the permission is
// synchronized if the scheduling changes.
func SyncPermission(mutexes *system.Mutexes, machine *labcloud.Machine, permission *labcloud.Permission) {
	for {
		duration := 5 * time.Second

		mutexes.Machine.Lock()
		mutexes.Permission.Lock()
		if machine.ID != "" {
			updatedPermission := labcloud.ReadPermission(machine.Permission)

			if updatedPermission.ID != "" {
				*permission = updatedPermission
				duration = 5 * time.Minute
			}
		}

		mutexes.Permission.Unlock()
		mutexes.Machine.Unlock()
		time.Sleep(duration)
	}
}

// CheckAccess will check if the user has the required permission
func CheckAccess(mutexes *system.Mutexes, user *labcloud.User, machine *labcloud.Machine) bool {
	mutexes.Machine.Lock()
	// Check if the user is an Admin or Staff
	if user.Role == "admin" || user.Role == "staff" {
		mutexes.Machine.Unlock()
		return true
	}
	for _, permission := range user.Permissions {
		if permission == machine.Permission {
			mutexes.Machine.Unlock()
			return true
		}
	}
	mutexes.Machine.Unlock()
	return false
}

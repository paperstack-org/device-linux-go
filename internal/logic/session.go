package logic

import (
	"log"
	"time"

	"gitlab.com/sdu-labcloud/node-linux-go/internal/http"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/labcloud"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/led"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/system"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/tmpstore"
)

// RegisterSession will either resume an existing session by
// loading the API token from memory or create a new session
// in the API.
func RegisterSession(nodeID string) labcloud.Session {
	// Attempt to read existing session from memory.
	cache := tmpstore.Get()

	// Check if an existing session exists and if it has an API key.
	if cache.Session.Token == "" || cache.Session.Node != nodeID {
		// Create a new session if the cache does not contain a valid one.
		log.Println("Creating new session ...")
		cache.Session = labcloud.CreateSession(nodeID)
		tmpstore.Set(cache)
	} else {
		log.Println("Resuming old session ...")
	}

	// Set token for authorized HTTP calls.
	http.Token = cache.Session.Token

	// Return the session.
	return cache.Session
}

// AwaitSessionAuthorization waits for the session to be
// authorized to gain API access.
func AwaitSessionAuthorization(mutexes *system.Mutexes, session *labcloud.Session) {
	// Read current session until it is authorized.
	for !session.Authorized {
		mutexes.Session.Lock()
		updatedSession := labcloud.ReadSession(session.Node, session.ID)

		if updatedSession.ID != "" {
			*session = updatedSession
		}
		mutexes.Session.Unlock()

		led.Static(led.ColorOrange)
		time.Sleep(time.Second)
	}

	// Write session upon successful authorization.
	cache := tmpstore.Get()
	cache.Session.Authorized = true
	tmpstore.Set(cache)
}

// HandleSessionIdentify periodically checks the API to
// see if the node should identify itself.
func HandleSessionIdentify(mutexes *system.Mutexes, session *labcloud.Session) {
	for {
		mutexes.Session.Lock()
		updatedSession := labcloud.ReadSession(session.Node, session.ID)
		if updatedSession.ID == "" {
			// Shut down node if the session was deleted.
			mutexes.Update.Lock()
			log.Fatalln("error: session was deleted")
		}
		*session = updatedSession

		if session.Identify {
			led.Identify(true)
		} else {
			led.Identify(false)
		}
		mutexes.Session.Unlock()

		time.Sleep(time.Second * 5)
	}
}

// +build arm

package rfid

import (
	"bufio"
	"encoding/hex"
	"io"
	"log"
	"strings"
	"time"

	"github.com/tarm/serial"
)

const (
	port        = "/dev/serial0"
	baud        = 9600
	readTimeout = time.Millisecond * 500
)

// Create serial port handle.
var serialPort *serial.Port

// Create reader handle
var reader *bufio.Reader

// init will configure and open the serial port.
func init() {
	// Create serial port configuration.
	serialConfig := &serial.Config{Name: port, Baud: baud, ReadTimeout: readTimeout}

	// Open serial port.
	var err error
	serialPort, err = serial.OpenPort(serialConfig)
	if err != nil {
		log.Fatalf("error: opening serial port failed: %s: %s", port, err)
	}

	// Create reader for serial port
	reader = bufio.NewReader(serialPort)
}

// readTagID will return the RFID tag ID from the most recent RFID tag.
func readTagID() string {
	// Create IO buffer and read available data from serial port.
	buffer := make([]byte, 5)

	// Read from serial port.
	n, err := io.ReadAtLeast(serialPort, buffer, 5)
	if err != nil {
		// Ignore EOF errors as this will happen if no card is present.
		if err == io.EOF {
			log.Println("RFID: No bytes read")
			return ""
		}
		if err == io.ErrUnexpectedEOF {
			log.Println("RFID: ID incomplete")
			return ""
		}
		log.Fatalf("error: reading from serial port failed: %s: %s", port, err)
	}

	// Flush serial port to clear unread data.
	reader.Discard(reader.Buffered())
	// err = reader.Reset(serialPort)
	// if err != nil {
	// 	log.Printf("error: flushing serial port failed: %s: %s", port, err)
	// }

	// If less than 5 bytes were read, the ID is incomplete.
	if n < 5 {
		log.Println("RFID: ID incomplete, read", n, "bytes")
		return ""
	}

	// If less than 10 bytes were read, only consider the last 5 bytes.
	if n < 10 {
		read := hex.EncodeToString(buffer[n-5 : n])
		log.Println("RFID: 10 Bytes read, returning", strings.ToUpper(read[2:]))
		return strings.ToUpper(read[2:])
	}

	// Check if the tag was present for the last two read cycles.
	// Consider the last two reads to minimize the chance of
	// confusing 7 byte RFID tag IDs for 4 byte RFID tag IDs.
	readA := hex.EncodeToString(buffer[n-10 : n-5])
	readB := hex.EncodeToString(buffer[n-5 : n])
	if readA != readB {
		log.Println("RFID: Read A != Read B   ", readA, "!=", readB)
		return ""
	}

	// Remove tag type information.
	log.Println("RFID: Card read:", strings.ToUpper(readB[2:]))
	return strings.ToUpper(readB[2:])
}

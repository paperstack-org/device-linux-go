package rfid

import (
	"time"
)

var (
	// ReadInterval defines the minimum inteval between two read operations.
	ReadInterval = time.Millisecond * 200
	// TagID is the last read RFID tag ID.
	TagID string
)

// Poll reads the RFID tag ID in the background.
func Poll() {
	for {
		// Read and set RFID tag ID.
		TagID = readTagID()

		// Delay next read.
		time.Sleep(ReadInterval)
	}
}

// +build !arm

package rfid

import (
	"log"
	"os"
	"strings"

	"github.com/joho/godotenv"
)

// readTagID is a mock for reading the RFID tag ID.
// It will read the RFID tag ID specified in the
// MOCK_RFID_UID environment variable in the ".env" file.
func readTagID() string {
	// Load values from ".env" file.
	err := godotenv.Overload()
	if err != nil {
		log.Fatalf("error: loading .env file failed: %s", err)
	}

	// Return value of mock environment variable.
	return strings.TrimSpace(os.Getenv("MOCK_RFID_UID"))
}

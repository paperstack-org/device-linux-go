// +build arm

package system

import (
	"bufio"
	"log"
	"os"
	"strings"
)

// Serial returns the systems serial number of a Broadcom chip.
func Serial() string {
	// Open CPU information file.
	file, err := os.Open("/proc/cpuinfo")
	if err != nil {
		log.Fatalf("error: opening CPU information failed: %s", err)
	}

	// Ensure that the file is closed, once the function returns.
	defer file.Close()

	// Create new scanner to process file line by line.
	scanner := bufio.NewScanner(file)
	// Iterate over all lines in the file.
	for scanner.Scan() {
		line := scanner.Text()
		// Check if the line contains information about the serial number.
		if strings.HasPrefix(line, "Serial") {
			// Extract serial number.
			serialInfo := strings.Split(line, ":")
			return strings.ToUpper(strings.TrimSpace(serialInfo[1]))
		}
	}

	// Handle scanner errors.
	if err := scanner.Err(); err != nil {
		log.Fatalf("error: reading CPU information failed: %s", err)
	}

	// Return well-known string if serial number cannot be loaded.
	return "NO_SERIAL"
}

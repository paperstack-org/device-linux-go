package system

import (
	"log"
	"net"
	"strings"
)

// ActiveNetworkInterface returns an active network interface.
// Wi-Fi interfaces are preferred over ethernet interfaces.
func ActiveNetworkInterface() *net.Interface {
	// Get all interfaces.
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Fatalf("error: reading network interfaces failed: %s", err)
	}

	// Get all active wireless and ethernet interfaces.
	var upWirelesses []net.Interface
	var upEthernets []net.Interface
	for _, iface := range ifaces {
		// Check if the interface is the loopback interface.
		if !strings.HasPrefix(iface.Name, "lo") {
			// Check if the interface is active.
			if iface.Flags&net.FlagUp != 0 {
				// Check if the interface is a wireless interface.
				if strings.HasPrefix(iface.Name, "w") {
					upWirelesses = append(upWirelesses, iface)
				}
				// Check if the interface is an ethernet interface.
				if strings.HasPrefix(iface.Name, "e") {
					upEthernets = append(upEthernets, iface)
				}
			}
		}
	}

	// Return the first active wireless interface, if any.
	if len(upWirelesses) != 0 {
		return &upWirelesses[0]
	}

	// Return the first active ethernet interface, if any.
	if len(upEthernets) != 0 {
		return &upEthernets[0]
	}

	log.Fatalf("error: no active interface found")
	return nil
}

// NormalizeMAC returns are normalized MAC address.
// That is, the MAC address will not contain any colons
// and it will be capitalized.
func NormalizeMAC(rawMAC *net.HardwareAddr) string {
	// Convert byte array to string, remove all colons and capitalize all lowercase letters.
	return strings.ToUpper(strings.ReplaceAll(rawMAC.String(), ":", ""))
}

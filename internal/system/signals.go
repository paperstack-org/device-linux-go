package system

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/sdu-labcloud/node-linux-go/internal/gpio"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/led"
)

// Receive OS signals and handle them.
var signals chan os.Signal

// HandleSignals handles process interrupt signals.
func HandleSignals(mutexes *Mutexes) {
	// Configure channel for OS signals.
	signals = make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT)

	// Wait for signals.
	<-signals

	// Print new line to prevent messing with the shell.
	fmt.Println()

	// Reset drivers.
	led.Reset()
	gpio.Reset()

	// Ensure that the node is not currently updating.
	mutexes.Update.Lock()

	// Exit program.
	os.Exit(0)
}

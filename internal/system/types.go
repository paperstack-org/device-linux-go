package system

import (
	"sync"
)

// Mutexes controls access to shared state within the application.
type Mutexes struct {
	Update     sync.Mutex
	Node       sync.Mutex
	Session    sync.Mutex
	Machine    sync.Mutex
	User       sync.Mutex
	Permission sync.Mutex
}

package tmpstore

import (
	"encoding/json"
	"log"
	"os"
)

var (
	tmpstoreFilePath = os.TempDir() + "/node-linux-go.json"
	cache            = CachedData{}
)

// Set writes to the tmpstore. The data
// will be persisted between application
// restarts, but not reboots.
func Set(newCachedData *CachedData) {
	// Ensure that the tmpstore file exists.
	tmpstoreFile, err := os.OpenFile(tmpstoreFilePath, os.O_RDWR|os.O_CREATE, 0600)
	if err != nil {
		log.Fatalf("error: opening tmpstore failed: %s", err)
	}

	// Write JSON file.
	writeJSONFile(tmpstoreFile, newCachedData)
}

// Get fetches  variable from the tmpstore.
func Get() *CachedData {
	// Ensure that the tmpstore file exists.
	tmpstoreFile, err := os.Open(tmpstoreFilePath)
	if err != nil {
		// Return nothing if the file does not exist yet.
		return &cache
	}

	return readJSONFile(tmpstoreFile)
}

// readJSONFile will decode file content as JSON and
// return a map with string keys and arbitrary key types.
func readJSONFile(file *os.File) *CachedData {
	// Read information of tmpstore file.
	fileInfo, err := file.Stat()
	if err != nil {
		log.Fatalf("error: probing tmpstore failed: %s", err)
	}

	// Check if file is empty.
	if fileInfo.Size() != 0 {
		// Decode JSON string to read data.
		if err := json.NewDecoder(file).Decode(&cache); err != nil {
			log.Fatalf("error: decoding tmpstore failed: %s", err)
		}
	} else {
		log.Printf("Creating tmpstore: %s \n", tmpstoreFilePath)

		// Initialize the map if the tmpstore is empty.
		cache = CachedData{}
	}

	return &cache
}

// writeJSONFile will encode a map with string keys and
// arbitrary key types to a string and write it to the
// given file.
func writeJSONFile(file *os.File, data *CachedData) {
	// Render new file content.
	fileDataString, err := json.MarshalIndent(*data, "", "  ")
	if err != nil {
		log.Fatalf("error: encoding tmpstore failed: %s", err)
	}

	// Clear file.
	if err = file.Truncate(0); err != nil {
		log.Fatalf("error: updating tmpstore failed: %s", err)
	}

	// Move cursor to beginning of file.
	if _, err = file.Seek(0, 0); err != nil {
		log.Fatalf("error: updating tmpstore failed: %s", err)
	}

	// Create tmpstore file content and append unix style line ending.
	_, err = file.WriteString(string(fileDataString) + "\n")
	if err != nil {
		log.Fatalf("error: writing tmpstore failed: %s", err)
	}

	// Close file.
	err = file.Close()
	if err != nil {
		log.Fatalf("error: closing tmpstore failed: %s", err)
	}
}

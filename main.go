package main

import (
	"log"
	"time"

	"gitlab.com/sdu-labcloud/node-linux-go/internal/gpio"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/labcloud"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/led"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/logic"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/rfid"
	"gitlab.com/sdu-labcloud/node-linux-go/internal/system"
)

// Manage access to shared resources.
var mutexes system.Mutexes

// init will configure logging to use the UTC time format.
func init() {
	log.SetFlags(log.LstdFlags | log.LUTC)
}

// main implements the core logic of the application.
func main() {
	// Comment to test Auto-update functionality
	// Handle system signals, such as SIGINT.
	go system.HandleSignals(&mutexes)

	// Set a static color upon startup.
	led.Static(led.ColorOrange)

	// Ensure that node exists in API.
	node := logic.RegisterNode()
	log.Println("Registered node: " + node.ID)

	// Use existing or obtain new session token.
	session := logic.RegisterSession(node.ID)
	log.Println("Registered session: " + session.ID)

	// Check for updates.
	var machine labcloud.Machine
	go system.CheckUpdate(&mutexes, &machine, &node)

	// Check periodically if Identify is active
	go logic.HandleSessionIdentify(&mutexes, &session)

	// Wait until session is authorized.
	logic.AwaitSessionAuthorization(&mutexes, &session)
	log.Println("Authorization granted!")

	// Update the node information.
	mutexes.Node.Lock()
	node.ApplicationName = "node-linux-go"
	node.ApplicationVersion = system.CommitSHA
	node = labcloud.UpdateNode(node.ID, node)
	mutexes.Node.Unlock()

	// Wait until the machine  if the node has a machine.
	{
		mutexes.Machine.Lock()
		updatedMachine := logic.AwaitMachineAssignment(&mutexes, &node)
		if updatedMachine.ID != "" {
			machine = updatedMachine
		}
		log.Println("Machine assigned!")
		mutexes.Machine.Unlock()
	}

	// Watch node to check if it has machine assigned and if it changed.
	go logic.CheckNodeMachine(&mutexes, &node)

	// Update the machine status to reflect that the machine can be used.
	{
		mutexes.Machine.Lock()
		updatedMachine := labcloud.UpdateMachineStatus(machine.ID, "ready")
		if updatedMachine.ID != "" {
			machine = updatedMachine
		}
		mutexes.Machine.Unlock()
	}
	led.Static(led.ColorWhite)

	// Watch machine status to see if it has changed
	go logic.CheckMachineStatus(&mutexes, &machine)

	// Read machine permission.
	var permission labcloud.Permission
	go logic.SyncPermission(&mutexes, &machine, &permission)

	for permission.Scheduling == "" {
		// Wait for permission to be valid
	}
	log.Println("Using scheduling scheme", permission.Scheduling)

	// Start reading the RFID tag.
	go rfid.Poll()

	for {
		// Read the ID of the presented RFID tag.
		tag := rfid.TagID
		if tag != "" {
			log.Println("Tag found:", tag)
		}

		switch permission.Scheduling {
		case "on_demand":
			// Scheduling where the user has to be constantly present.
			if tag != "" {
				user := labcloud.ReadUser(tag)
				if logic.CheckAccess(&mutexes, &user, &machine) {
					mutexes.Update.Lock() // Lock the update mutex to prevent updates while using the machine.

					for rfid.TagID == tag {
						led.Static(led.ColorGreen)
						gpio.RelayOn()
						time.Sleep(rfid.ReadInterval)
					}

					mutexes.Update.Unlock()
				}
			}
			gpio.RelayOff()
			led.Static(led.ColorWhite)
			time.Sleep(rfid.ReadInterval)

		case "locking":
			// Scheduling where the device is used like a locker.
			if tag != "" {
				user := labcloud.ReadUser(tag)
				if logic.CheckAccess(&mutexes, &user, &machine) {
					mutexes.Update.Lock() // Lock the update mutex to prevent updates while using the machine.
					// Change the LED to green
					led.Static(led.ColorGreen)

					// Update machine status
					mutexes.Machine.Lock()
					machine = labcloud.UpdateMachineUser(machine.ID, user.ID)
					mutexes.Machine.Unlock()

					// Open the door
					gpio.LightStripOn()
					gpio.LockOpen()
					time.Sleep(time.Second)

					for !gpio.LockState() {
						// Wait until door is closed
					}
					gpio.LightStripOff()

					// Door remains locked until the tag is present or machine is released
					for newTag := rfid.TagID; newTag != tag; newTag = rfid.TagID {
						// If new card is present, check if admin or staff
						if newTag != "" {
							role := labcloud.ReadUser(newTag).Role
							if role == "admin" || role == "staff" {
								break
							}
						}
						// Check if status of the machine has been changed via web interface
						if machine.Status != "busy" {
							break
						}
						time.Sleep(rfid.ReadInterval)
						led.Static(led.ColorRed)
					}

					// If the machine status is not busy, it was changed from the web interface, so we don't open the door.
					if machine.Status == "busy" {
						// Open the door
						led.Static(led.ColorGreen)
						gpio.LightStripOn()
						gpio.LockOpen()
						time.Sleep(time.Second)

						for !gpio.LockState() {
							// Wait until door is closed
						}
						gpio.LightStripOff()

						// Update machine status
						mutexes.Machine.Lock()
						machine = labcloud.UpdateMachineStatus(machine.ID, "ready")
						mutexes.Machine.Unlock()
					}
					mutexes.Update.Unlock()
				} else {
					log.Print("Denied: ", tag)
					led.Blink(led.ColorRed)
					time.Sleep(time.Second)
				}
			}
			led.Static(led.ColorWhite)
			time.Sleep(rfid.ReadInterval)

		default:
			log.Println("Unknown scheduling scheme:", permission.Scheduling)
			led.Static(led.ColorPurple)
		}
	}
}

#!/usr/bin/env bash

# Global variables.
groupName="sdu-labcloud"
appName="node-linux-go"
installDir="/opt/${appName}"
installUser="root"

# Color variables.
default='\e[00;0m'
red='\e[00;31m'
green='\e[00;32m'
magenta='\e[00;35m'

# Color shortcuts.
d=${default}
r=${red}
g=${green}
m=${magenta}

# Message indicators.
info="${g}info:${d}   "
error="${r}error:${d}  "

# Check if user has sudo rights.
if [[ ${EUID} -ne 0 ]]; then
  echo -e "${error}This script must be run as ${m}root${d} or with ${m}sudo${d}."
  exit 1
fi

# Display error and exit program in case a command failed.
# First parameter is the return code of a program,
# which can be obtained by $? and the second parameter
# is a message in case the return code is non-zero.
# Usage: check_error $? "Something went wrong."
check_error() {
  if [ ${1} -ne 0 ]; then
    echo -e "${error}${2}"
    exit 1
  fi
}

# Detect CPU architecture.
linuxArch=$(uname -m)
arch="arm"
if [ "${linuxArch}" = "x86_64" ]; then
  arch="amd64"
fi

# Install application.
echo -e "${info}Installing application ..."
mkdir -p ${installDir}
chown -R ${installUser}:${installUser} ${installDir}
wget -q "https://gitlab.com/${groupName}/${appName}/-/jobs/artifacts/master/raw/${appName}-${arch}?job=compile-${arch}" -O ${installDir}/${appName} && chmod +x ${installDir}/${appName}
check_error $? "Failed to install application."
cp /tmp/usb-provisioning/environment.ini ${installDir}/.env
if [ ! -f "${installDir}/.env" ]; then
  touch ${installDir}/.env
fi

# Install daemon.
echo -e "${info}Installing systemd service..."
wget -q https://gitlab.com/${groupName}/${appName}/-/raw/master/scripts/${appName}.service -O /lib/systemd/system/${appName}.service
check_error $? "Failed to download service definition: ${m}${appName}${d}"

# Enable and start daemon.
systemctl enable --now "${appName}" &> /dev/null
check_error $? "Failed to enable service at startup: ${m}${appName}${d}"
echo -e "${info}Successfully installed application: ${m}${appName}${d}"
